function TruthTable(trueSymbol, falseSymbol, binaryFunction) {

    this.trueSymbol = trueSymbol;
    this.falseSymbol = falseSymbol;
    this.binaryFunction = binaryFunction;

    this.draw = function (context, xPos, yPos, width, height, highlightedRow) {
        context.clearRect(xPos, yPos, width, height);

        var columnWidth = width / 3;
        var rowHeight = height / 5;

        var headerLineY = yPos + rowHeight;
        var lastColumnSeparatorX = xPos + width - columnWidth;

        //highlight selected row
        highlightedRow++; //adjustment because the drawSymbol method indexes the header row with 0
        context.fillStyle = "yellow";
        context.fillRect(xPos, yPos + highlightedRow * rowHeight, width, rowHeight);


        //draw table layout
        context.strokeWidth = 2;
        context.moveTo(xPos, headerLineY);
        context.lineTo(xPos + width, headerLineY);
        context.stroke();

        context.moveTo(lastColumnSeparatorX, yPos);
        context.lineTo(lastColumnSeparatorX, yPos + height);
        context.stroke();

        context.fillStyle = "black";
        var fontSize = width /6;
        context.font = fontSize + "px Ariel";

        function drawSymbol(symbol, row, column) {
            var x = xPos + column * columnWidth;
            var y = yPos + row * rowHeight;

            context.fillText(
                    symbol,
                    x + columnWidth / 2,
                    y + rowHeight / 2
            );
        }

        var previousBaseline = context.textBaseline;
        var previousAlign = context.textAlign;

        context.textBaseline = "middle";
        context.textAlign = "center";

        drawSymbol('A', 0, 0);
        drawSymbol('B', 0, 1);
        drawSymbol('f', 0, 2);

        var row = 1;
        for (var i = 0; i <= 1; i++) {
            for (var j = 0; j <= 1; j++) {
                drawSymbol(i != 0 ? this.trueSymbol : this.falseSymbol, row, 0);
                drawSymbol(j != 0 ? this.trueSymbol : this.falseSymbol, row, 1);
                drawSymbol(this.binaryFunction(i, j) ? this.trueSymbol : this.falseSymbol, row, 2);
                row++;
            }
        }

        context.textBaseline = previousBaseline;
        context.textAlign = previousAlign;
    };
}

function DiodeANDCircuit(diodeVoltage, lowVoltage, highVoltage) {

    this.diodeVoltage = diodeVoltage;
    this.lowVoltage = lowVoltage;
    this.highVoltage = highVoltage;
    this.sourceVoltage = highVoltage;

    this.inputA = false;
    this.inputB = false;
    this.output = false;

    this.inputAConnector = undefined;
    this.inputBConnector = undefined;
    this.outputConnector = undefined;

    this.setInputA = function (inputA) {
        this.inputA = inputA;
        this.recalculateVoltages();
    };

    this.setInputB = function (inputB) {
        this.inputB = inputB;
        this.recalculateVoltages();
    };

    this.recalculateVoltages = function () {
        if (this.inputA && this.inputB) {
            this.voltageF = this.highVoltage;
            this.output = true;
        } else {
            this.voltageF = this.lowVoltage + this.diodeVoltage;
            this.output = false;
        }
    };

    this.recalculateVoltages();

    this.draw = function(context, xPos, yPos, boxWidth, boxHeight, toAnimate) {
        context.clearRect(xPos, yPos, boxWidth, boxHeight);
        window.clearTimeout(this.timeoutID);

        //enclose the circuit in a box
        context.lineWidth = 3;
        context.strokeRect(xPos, yPos, boxWidth, boxHeight);

        // context.fillStyle = "orange";
        // context.fillRect(xPos, yPos, boxWidth, boxHeight);

        context.fillStyle = "black";
        context.strokeStyle = "black";
        context.lineWidth = 1;

        var voltageLineX = xPos + boxWidth / 2;
        var inputALineY = yPos + (3 / 7) * boxHeight;
        var inputBLineY = yPos + (4 / 7) * boxHeight;

        //line that input A comes from
        context.moveTo(xPos, inputALineY);
        context.lineTo(voltageLineX, inputALineY);
        context.stroke();

        //line that input B comes from
        context.moveTo(xPos, inputBLineY);
        context.lineTo(xPos + boxWidth, inputBLineY);
        context.stroke();

        //line that the voltage comes from
        context.moveTo(voltageLineX, yPos);
        context.lineTo(voltageLineX, inputBLineY);
        context.stroke();

        //line that is grounded
        var groundLineY = yPos + (15 / 16) * boxHeight;
        context.moveTo(xPos, groundLineY);
        context.lineTo(xPos + boxWidth, groundLineY);
        context.stroke();

        //draw diodes
        var diodeTriangleBase = boxHeight / 12;
        var diodeTriangleHeight = boxWidth / 12;

        var boundBoxDiodeA = new Rectangle(
                xPos + boxWidth / 4 - diodeTriangleHeight,
                inputALineY - diodeTriangleBase / 2,
                diodeTriangleHeight,
                diodeTriangleBase
        );
        drawDiodeToLeft(context, boundBoxDiodeA);

        var boundBoxDiodeB = new Rectangle(
                xPos + boxWidth / 4 - diodeTriangleHeight,
                inputBLineY - diodeTriangleBase / 2,
                diodeTriangleHeight,
                diodeTriangleBase
        );
        drawDiodeToLeft(context, boundBoxDiodeB);

        //draw wire joinings
        (new Circle(voltageLineX, inputALineY, 3)).draw(context, true);
        (new Circle(voltageLineX, inputBLineY, 3)).draw(context, true);
        (new Circle(voltageLineX, groundLineY, 3)).draw(context, true);

        //draw resistor
        var resistorHeight = boxHeight / 6;
        var resistorWidth = boxWidth / 12;
        var resistorRectangle = new Rectangle(
                voltageLineX - resistorWidth / 2,
                (yPos + inputALineY - resistorHeight) / 2,
                resistorWidth,
                resistorHeight
        );

        context.fillStyle = "white";
        resistorRectangle.draw(context, true);

        //draw grounding
        var groundSymbolY = yPos + boxHeight + boxHeight / 20;
        context.moveTo(voltageLineX, groundLineY);
        context.lineTo(voltageLineX, groundSymbolY);
        context.stroke();

        var groundSymbolWidth = boxWidth / 12;
        context.lineWidth = 2;
        context.moveTo(voltageLineX - groundSymbolWidth / 2, groundSymbolY);
        context.lineTo(voltageLineX + groundSymbolWidth / 2, groundSymbolY);
        context.stroke();

        //draw voltage arrow
        context.lineWidth = 1;
        context.fillStyle = "black";

        var voltageTriangleBaseY = yPos - boxHeight / 20;
        context.moveTo(voltageLineX, yPos);
        context.lineTo(voltageLineX, voltageTriangleBaseY);
        context.stroke();

        var voltageTriangleBase = boxWidth / 12;
        var voltageTriangleHeight = boxHeight / 20;

        var voltageTriangle = new Triangle(
                voltageLineX - voltageTriangleBase / 2,
                voltageTriangleBaseY,
                voltageLineX + voltageTriangleBase / 2,
                voltageTriangleBaseY,
                voltageLineX,
                voltageTriangleBaseY - voltageTriangleHeight
        );
        voltageTriangle.draw(context, true);

        //draw wire connections to outside the circuit
        context.fillStyle = "white";
        var connectionCircleRadius = 3;

        var inputACircle = new Circle(xPos, inputALineY, connectionCircleRadius);
        inputACircle.draw(context, true);

        var inputBCircle = new Circle(xPos, inputBLineY, connectionCircleRadius);
        inputBCircle.draw(context, true);

        this.inputAConnector = new Point(xPos, inputALineY);
        this.inputBConnector = new Point(xPos, inputBLineY);
        this.outputConnector = new Point(xPos + boxWidth, inputBLineY);

        (new Circle(this.inputAConnector.x, this.inputAConnector.y, connectionCircleRadius)).draw(context, true);
        (new Circle(this.inputBConnector.x, this.inputBConnector.y, connectionCircleRadius)).draw(context, true);
        (new Circle(this.outputConnector.x, this.outputConnector.y, connectionCircleRadius)).draw(context, true);

        this.inputAConnector.x -= connectionCircleRadius;
        this.inputBConnector.x -= connectionCircleRadius;
        this.outputConnector.x += connectionCircleRadius;

        //draw voltage values
        var fontSize = Math.min(boxHeight, boxWidth) / 10;
        context.font = fontSize + 'px Ariel';
        context.fillStyle = "black";
        context.textAlign = "left";

        context.textBaseline = "middle";
        context.fillText(this.voltageF + 'V', voltageLineX + 10, inputALineY);

        var voltageString = this.sourceVoltage + "V";
        context.fillText(voltageString, voltageLineX + voltageTriangleBase, voltageTriangleBaseY - voltageTriangleHeight / 2);

        context.textBaseline = "bottom";
        var inputAVoltage = this.inputA ? this.highVoltage : this.lowVoltage;
        context.fillText(inputAVoltage + 'V', xPos + 5, inputALineY);

        context.textBaseline = "top";
        var inputBVoltage = this.inputB ? this.highVoltage : this.lowVoltage;
        context.fillText(inputBVoltage + 'V', xPos + 5, inputBLineY);

        //draw voltage drops
        drawArrowUp(context, xPos + 3 * boxWidth / 4, groundLineY - 10, groundLineY - inputBLineY - 20);
        drawArrowUp(
            context,
            resistorRectangle.x + resistorRectangle.width * 2,
            resistorRectangle.y + resistorRectangle.height + 10,
            resistorRectangle.height + 20
        );
        drawArrowRight(
            context,
            boundBoxDiodeA.x - 5,
            boundBoxDiodeA.y - 10,
            boundBoxDiodeA.width + 15
        );
        drawArrowRight(
            context,
            boundBoxDiodeB.x - 5,
            boundBoxDiodeB.y + boundBoxDiodeB.height + 10,
            boundBoxDiodeB.width + 15
        );

        context.textAlign = "left";
        context.textBaseline = "middle";

        context.fillText(
            this.voltageF + 'V',
            xPos + 3 * boxWidth / 4 + 5,
            (groundLineY + inputBLineY) / 2
        );
        var resistorVoltage = this.highVoltage - this.voltageF;
        context.fillText(
            resistorVoltage + 'V',
            resistorRectangle.x + resistorRectangle.width * 2 + 5,
            resistorRectangle.y + resistorRectangle.height / 2
        );

        context.textAlign = "center";
        context.textBaseline = "bottom";
        var diodeAVoltage = this.voltageF - inputAVoltage;
        context.fillText(
            diodeAVoltage + 'V',
            boundBoxDiodeA.x + boundBoxDiodeA.width / 2,
            boundBoxDiodeA.y - 15
        );

        context.textBaseline = "top";
        var diodeBVoltage = this.voltageF - inputBVoltage;
        context.fillText(
            diodeBVoltage + 'V',
            boundBoxDiodeB.x + boundBoxDiodeB.width / 2,
            boundBoxDiodeB.y + boundBoxDiodeB.height + 15
        );

        //animations
        this.animate = function (counter) {
            if (this.inputA && this.inputB) return;

            context.strokeStyle = "black";
            context.setLineDash([]);

            //redraw the black part of the lines
            context.moveTo(voltageLineX, voltageTriangleBaseY);
            context.lineTo(voltageLineX, resistorRectangle.y);
            context.stroke();

            context.moveTo(voltageLineX, resistorRectangle.y + resistorRectangle.height);
            context.lineTo(voltageLineX, inputBLineY);
            context.stroke();

            if (!this.inputA) {
                context.moveTo(voltageLineX, inputALineY);
                context.lineTo(boundBoxDiodeA.x + boundBoxDiodeA.width, inputALineY);
                context.stroke();

                context.moveTo(boundBoxDiodeA.x, inputALineY);
                context.lineTo(xPos + connectionCircleRadius, inputALineY);
                context.stroke();
            }

            if (!this.inputB) {
                context.moveTo(voltageLineX, inputALineY);
                context.lineTo(voltageLineX, inputBLineY);
                context.stroke();

                context.moveTo(voltageLineX, inputBLineY);
                context.lineTo(boundBoxDiodeB.x + boundBoxDiodeB.width, inputBLineY);
                context.stroke();

                context.moveTo(boundBoxDiodeB.x, inputBLineY);
                context.lineTo(xPos + connectionCircleRadius, inputBLineY);
                context.stroke();
            }

            var offset = counter % 10;

            context.strokeStyle = "yellow";
            context.setLineDash([5, 5]);

            //draw moving yellow parts
            context.beginPath();
            context.moveTo(voltageLineX, voltageTriangleBaseY + offset);
            context.lineTo(voltageLineX, resistorRectangle.y);
            context.stroke();

            context.beginPath();
            context.moveTo(voltageLineX, resistorRectangle.y + resistorRectangle.height + offset);
            context.lineTo(voltageLineX, inputALineY - connectionCircleRadius);
            context.stroke();

            if (!this.inputA) {
                context.moveTo(voltageLineX - connectionCircleRadius - offset, inputALineY);
                context.lineTo(boundBoxDiodeA.x + boundBoxDiodeA.width, inputALineY);
                context.stroke();

                context.moveTo(boundBoxDiodeA.x - offset, inputALineY);
                context.lineTo(xPos + connectionCircleRadius, inputALineY);
                context.stroke();
            }

            if (!this.inputB) {
                context.moveTo(voltageLineX, inputALineY + offset);
                context.lineTo(voltageLineX, inputBLineY);
                context.stroke();

                context.moveTo(voltageLineX - connectionCircleRadius - offset, inputBLineY);
                context.lineTo(boundBoxDiodeB.x + boundBoxDiodeB.width, inputBLineY);
                context.stroke();

                context.moveTo(boundBoxDiodeB.x - offset, inputBLineY);
                context.lineTo(xPos + connectionCircleRadius, inputBLineY);
                context.stroke();
            }

            context.setLineDash([]);
            context.strokeStyle = "black";

            var that = this;
            this.timeoutID = window.setTimeout(function () {
                that.animate(offset + 1);
            }, 40);
        };

        if (toAnimate) {
            this.animate(0);
        }
    }
}

function DiodeORCircuit(diodeVoltage, lowVoltage, highVoltage) {

    this.diodeVoltage = diodeVoltage;
    this.lowVoltage = lowVoltage;
    this.highVoltage = highVoltage;
    this.sourceVoltage = highVoltage;

    this.inputA = false;
    this.inputB = false;
    this.output = false;

    this.inputAConnector = undefined;
    this.inputBConnector = undefined;
    this.outputConnector = undefined;

    this.setInputA = function (inputA) {
        this.inputA = inputA;
        this.recalculateVoltages();
    };

    this.setInputB = function (inputB) {
        this.inputB = inputB;
        this.recalculateVoltages();
    };

    this.recalculateVoltages = function () {
        if (this.inputA || this.inputB) {
            this.voltageF = this.highVoltage - this.diodeVoltage;
            this.output = true;
        } else {
            this.voltageF = this.lowVoltage;
            this.output = false;
        }
    };

    this.recalculateVoltages();

    this.draw = function (context, xPos, yPos, boxWidth, boxHeight, toAnimate) {
        context.clearRect(xPos, yPos, boxWidth, boxHeight);
        window.clearTimeout(this.timeoutID);

        //enclose the circuit in a box
        context.lineWidth = 3;
        context.strokeRect(xPos, yPos, boxWidth, boxHeight);

        context.fillStyle = "black";
        context.strokeStyle = "black";
        context.lineWidth = 1;

        var resistorLineX = xPos + (4 / 7) * boxWidth;
        var inputALineY = yPos + (2 / 7) * boxHeight;
        var inputBLineY = yPos + (3 / 7) * boxHeight;
        var groundLineY = yPos + (15 / 16) * boxHeight;

        //line that input A comes from
        context.moveTo(xPos, inputALineY);
        context.lineTo(resistorLineX, inputALineY);
        context.stroke();

        //line that input B comes from
        context.moveTo(xPos, inputBLineY);
        context.lineTo(xPos + boxWidth, inputBLineY);
        context.stroke();

        //line with the resistor
        context.moveTo(resistorLineX, inputALineY);
        context.lineTo(resistorLineX, groundLineY);
        context.stroke();

        //line that is grounded
        context.moveTo(xPos, groundLineY);
        context.lineTo(xPos + boxWidth, groundLineY);
        context.stroke();

        //draw diodes
        var diodeTriangleBase = boxHeight / 12;
        var diodeTriangleHeight = boxWidth / 12;

        var boundBoxDiodeA = new Rectangle(
            xPos + boxWidth / 4 ,
            inputALineY - diodeTriangleBase / 2,
            diodeTriangleHeight,
            diodeTriangleBase
        );
        drawDiodeToRight(context, boundBoxDiodeA);

        var boundBoxDiodeB = new Rectangle(
            xPos + boxWidth / 4,
            inputBLineY - diodeTriangleBase / 2,
            diodeTriangleHeight,
            diodeTriangleBase
        );
        drawDiodeToRight(context, boundBoxDiodeB);

        var connectionCircleRadius = 3;

        //draw wire joinings
        (new Circle(resistorLineX, inputBLineY, connectionCircleRadius)).draw(context, true);
        (new Circle(resistorLineX, groundLineY, connectionCircleRadius)).draw(context, true);

        //draw resistor
        var resistorHeight = boxHeight / 6;
        var resistorWidth = boxWidth / 12;
        var resistorRectangle = new Rectangle(
            resistorLineX - resistorWidth / 2,
            (inputBLineY + groundLineY - resistorHeight) / 2,
            resistorWidth,
            resistorHeight
        );

        context.fillStyle = "white";
        resistorRectangle.draw(context, true);

        //draw grounding
        var groundSymbolY = yPos + boxHeight + boxHeight / 20;
        context.moveTo(resistorLineX, groundLineY);
        context.lineTo(resistorLineX, groundSymbolY);
        context.stroke();

        var groundSymbolWidth = boxWidth / 12;
        context.lineWidth = 2;
        context.moveTo(resistorLineX - groundSymbolWidth / 2, groundSymbolY);
        context.lineTo(resistorLineX + groundSymbolWidth / 2, groundSymbolY);
        context.stroke();

        //draw wire connections to outside the circuit
        context.fillStyle = "white";

        var inputACircle = new Circle(xPos, inputALineY, connectionCircleRadius);
        inputACircle.draw(context, true);

        var inputBCircle = new Circle(xPos, inputBLineY, connectionCircleRadius);
        inputBCircle.draw(context, true);

        this.inputAConnector = new Point(xPos, inputALineY);
        this.inputBConnector = new Point(xPos, inputBLineY);
        this.outputConnector = new Point(xPos + boxWidth, inputBLineY);

        (new Circle(this.inputAConnector.x, this.inputAConnector.y, connectionCircleRadius)).draw(context, true);
        (new Circle(this.inputBConnector.x, this.inputBConnector.y, connectionCircleRadius)).draw(context, true);
        (new Circle(this.outputConnector.x, this.outputConnector.y, connectionCircleRadius)).draw(context, true);

        this.inputAConnector.x -= connectionCircleRadius;
        this.inputBConnector.x -= connectionCircleRadius;
        this.outputConnector.x += connectionCircleRadius;

        context.lineWidth = 1;

        //draw voltage values
        var fontSize = Math.min(boxHeight, boxWidth) / 10;
        context.font = fontSize + 'px Ariel';
        context.fillStyle = "black";

        context.textBaseline = "bottom";
        context.textAlign = "center";
        context.fillText(this.voltageF + 'V', (resistorLineX + xPos + boxWidth) / 2, inputBLineY);

        context.textAlign = "left";
        var inputAVoltage = this.inputA ? this.highVoltage : this.lowVoltage;
        context.fillText(inputAVoltage + 'V', xPos + 5, inputALineY);

        context.textBaseline = "top";
        var inputBVoltage = this.inputB ? this.highVoltage : this.lowVoltage;
        context.fillText(inputBVoltage + 'V', xPos + 5, inputBLineY);

        //draw voltage drops
        drawArrowUp(context, resistorLineX + resistorWidth + 5, groundLineY - 10, groundLineY - inputBLineY - 20);
        drawArrowLeft(
            context,
            boundBoxDiodeA.x + boundBoxDiodeA.width + 10,
            boundBoxDiodeA.y - 10,
            boundBoxDiodeA.width + 15
        );
        drawArrowLeft(
            context,
            boundBoxDiodeB.x + boundBoxDiodeB.width + 10,
            boundBoxDiodeB.y + boundBoxDiodeB.height + 10,
            boundBoxDiodeB.width + 15
        );

        context.textAlign = "left";
        context.textBaseline = "middle";

        //resistor voltage
        context.fillText(
            this.voltageF + 'V',
            resistorLineX + resistorWidth + 10,
            (groundLineY + inputBLineY) / 2
        );

        context.textAlign = "center";
        context.textBaseline = "bottom";
        var diodeAVoltage = roundToDecimals(inputAVoltage - this.voltageF, 2);
        context.fillText(
            diodeAVoltage + 'V',
            boundBoxDiodeA.x + boundBoxDiodeA.width / 2,
            boundBoxDiodeA.y - 15
        );

        context.textBaseline = "top";
        var diodeBVoltage = roundToDecimals(inputBVoltage - this.voltageF, 2);
        context.fillText(
            diodeBVoltage + 'V',
            boundBoxDiodeB.x + boundBoxDiodeB.width / 2,
            boundBoxDiodeB.y + boundBoxDiodeB.height + 15
        );

        //animations
        this.animate = function (counter) {
            //redraw static parts of the lines
            if (this.voltageF > 0) {
                context.moveTo(resistorLineX, inputBLineY);
                context.lineTo(resistorLineX, resistorRectangle.y);
                context.stroke();

                context.moveTo(resistorLineX, resistorRectangle.y + resistorRectangle.height);
                context.lineTo(resistorLineX, groundLineY);
                context.stroke();
            }

            if (this.inputA) {
                context.moveTo(xPos, inputALineY);
                context.lineTo(boundBoxDiodeA.x, inputALineY);
                context.stroke();

                context.moveTo(boundBoxDiodeA.x + boundBoxDiodeA.width, inputALineY);
                context.lineTo(resistorLineX - connectionCircleRadius, inputALineY);
                context.stroke();

                context.moveTo(resistorLineX, inputALineY + connectionCircleRadius);
                context.lineTo(resistorLineX, inputBLineY - connectionCircleRadius);
                context.stroke();
            }

            if (this.inputB) {
                context.moveTo(xPos, inputBLineY);
                context.lineTo(boundBoxDiodeB.x, inputBLineY);
                context.stroke();

                context.moveTo(boundBoxDiodeB.x + boundBoxDiodeB.width, inputBLineY);
                context.lineTo(resistorLineX - connectionCircleRadius, inputBLineY);
                context.stroke();
            }

            var offset = counter % 10;

            context.strokeStyle = "yellow";
            context.setLineDash([5, 5]);

            //draw moving yellow lines
            if (this.voltageF > 0) {
                context.beginPath();
                context.moveTo(resistorLineX, inputBLineY + connectionCircleRadius + offset);
                context.lineTo(resistorLineX, resistorRectangle.y);
                context.stroke();

                context.beginPath();
                context.moveTo(resistorLineX, resistorRectangle.y + resistorRectangle.height + offset);
                context.lineTo(resistorLineX, groundLineY - connectionCircleRadius);
                context.stroke();
            }

            if (this.inputA) {
                context.moveTo(xPos + offset, inputALineY);
                context.lineTo(boundBoxDiodeA.x, inputALineY);
                context.stroke();

                context.moveTo(boundBoxDiodeA.x + boundBoxDiodeA.width + offset, inputALineY);
                context.lineTo(resistorLineX - connectionCircleRadius, inputALineY);
                context.stroke();

                context.moveTo(resistorLineX, inputALineY + connectionCircleRadius + offset);
                context.lineTo(resistorLineX, inputBLineY - connectionCircleRadius);
                context.stroke();
            }

            if (this.inputB) {
                context.beginPath();
                context.moveTo(xPos + offset, inputBLineY);
                context.lineTo(boundBoxDiodeB.x, inputBLineY);
                context.stroke();

                context.beginPath();
                context.moveTo(boundBoxDiodeB.x + boundBoxDiodeB.width + offset, inputBLineY);
                context.lineTo(resistorLineX - connectionCircleRadius, inputBLineY);
                context.stroke();
            }

            context.strokeStyle = "black";
            context.setLineDash([]);

            var that = this;
            this.timeoutID = window.setTimeout(function () {
                that.animate(offset + 1);
            }, 40);
        };

        if (toAnimate) {
            this.animate(0);
        }
    }
}

function drawDiodeToLeft(context, box) {
    var anodeX = box.x + box.width; //plus side
    var cathodeX = box.x; //minus side

    var triangleMiddleY = box.y + box.height / 2;

    var triangle = new Triangle(
            anodeX, triangleMiddleY + box.height / 2,
            anodeX, triangleMiddleY - box.height / 2,
            cathodeX, triangleMiddleY
    );
    triangle.draw(context, true);

    context.moveTo(cathodeX, triangleMiddleY - box.height / 2);
    context.lineTo(cathodeX, triangleMiddleY + box.height / 2);
    context.stroke();
}

function drawDiodeToRight(context, box) {
    var anodeX = box.x; //plus side
    var cathodeX = box.x + box.width; //minus side

    var triangleMiddleY = box.y + box.height / 2;

    var triangle = new Triangle(
        anodeX, triangleMiddleY + box.height / 2,
        anodeX, triangleMiddleY - box.height / 2,
        cathodeX, triangleMiddleY
    );
    triangle.draw(context, true);

    context.moveTo(cathodeX, triangleMiddleY - box.height / 2);
    context.lineTo(cathodeX, triangleMiddleY + box.height / 2);
    context.stroke();
}