
<p>Na ovoj ćemo se stranici osvrnuti na općenitu implementaciju Booleovih funkcija. Sklopovi koji izravno ostvaruju Booleovu funkciju i koje smo do sada obradili (diodni I i diodni ILI), kao i sklopovi koje ćemo još obraditi, mogu se svesti na tri konceptualne strukture sklopa:</p>

<ul>
<li>Sklop kod kojeg je izlaz pritegnut na napajanje (takozvanim <i>pull-up</i> otpornikom).</li>
<li>Sklop kod kojeg je izlaz pritegnut na masu (takozvanim <i>pull-down</i> otpornikom).</li>
<li>Sklop s dvije sklopke prema napajanju i masi.</li>
</ul>

<p>Važno je da razumijete, barem na načelnoj razini, koja je ideja iza svake od ovih implementacija. Pa pogledajmo ih redom.</p>

<h3>Izlaz s otporom za pritezanje na napajanje</h3>

<p>Struktura ovakvog sklopa prikazana je na slici 1.</p>

<figure class="mlFigure">
<img src="@#file#(of-pu.svg)" alt="Izlaz pritegnut na napajanje."><br>
<figcaption><span class="mlFigKey">Slika 1.</span> Izlaz pritegnut na napajanje.</figcaption>
</figure>

<p>Kod ove izvedbe digitalnog sklopa izlaz <i>f</i> otpornikom za pritezanje spojen je na izvor napajanja. Dodatno, izlaz je sklopkom S<sub>d</sub> povezan na masu. Radom sklopke na neki način upravljaju ulazi sklopa (koji ovdje nisu prikazani). Napon koji se pojavljuje na izlazu sklopa određen je izrazom:
$$
 U_f = U - I_R \cdot R
$$
gdje smo s \(I_R\) označili struju koja teče iz izvora napona <i>U</i> kroz otpornik <i>R</i> i koja prema Ohmovom zakonu na njemu stvara pad napona \(I_R \cdot R\). U situaciji kada je sklopka S<sub>d</sub> isključena, strujni krug je prekinut i ne teče struja. Stoga je \(I_R=0\) pa je \(U_f = U - I_R \cdot R = U\), odnosno napon na izlazu jednak je naponu napajanja (kažemo: pritegnut je na napon napajanja; od tuda i naziv ove izvedbe).
</p> 

<p>Ako sklopku S<sub>d</sub> uključimo, tada se ona (u idealnom slučaju) ponaša kao kratak spoj. Time će izlaz sklopa biti spojen na masu pa će na njemu biti niska naponska razina. Uočite da u ovom slučaju kroz sklop teče struja iznosa:
$$
 I_R = \frac{U_R}{R} = \frac{U-U_f}{R} = \frac{U-0V}{R} = \frac{U}{R}.
$$
Kod sklopova ostvarenih na ovaj način postoji ova asimetrija: u jednoj situaciji struja ne teče pa sklop ne disipira snagu dok u drugoj situaciji struja teče i sklop disipira snagu.
</p>

<p>Interaktivna simulacija rada ovakvog sklopa prikazana je u nastavku. Klikom miša možete uključiti odnosno isključiti sklopku. Pogledajte kako se mijenjaju naponi na elementima te na izlazu. U ovom konkretnom slučaju simulacije, stanjem sklopke upravlja varijabla A: kada je A=0, sklopka je isključena a kada je A=1, sklopka je uključena. To kao posljedicu ima invertorsko ponašanje. Kada bi neka druga kombinacija varijabli upravljala sklopkom, dobili bismo druge funkcije.</p>

<script>
@#page-include#(Utility.js)
@#page-include#(UnaryTruthTable.js)
@#page-include#(InverterCircuits.js)
</script>

<div style="text-align: center;">
<canvas id="pullUpResistorCircuitCanvas"  width="500" height="300" style="border: solid 1px black"></canvas><br>
<span class="mlSimAuthor">Autor simulacije: Leon Luttenberger</span>
</div>

<script>
(function(){
    var canvas = document.getElementById("pullUpResistorCircuitCanvas");
    var context = canvas.getContext("2d");

    var circuit = new PullUpResistorCircuit(0, 5);
    var truthTable = new TruthTable('1', '0', true);

    var fontSize = 20;

    repaint();

    function repaint() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        circuit.draw(context, 100, 25, 150, 250, true);

        context.fillStyle = "black";
        context.font =  fontSize + 'px Ariel';
        context.textBaseline = "middle";
        context.textAlign = "left";

        context.fillText(
                "f=" + (circuit.output ? '1' : '0'),
                circuit.outputConnector.x + 5,
                circuit.outputConnector.y
        );

        var selectedRow = circuit.input ? 1 : 0;
        truthTable.draw(context, 325, 40, 150, 125, selectedRow);
    }

    canvas.addEventListener("click", function (e) {
        var pos = getMousePos(canvas, e);

        if (circuit.transistorRectangle.containsPoint(pos.x, pos.y)) {
            circuit.setInput(!circuit.input);
            repaint();
        }

    }, false);
})();
</script> 

<p>Diodni sklop I primjer je ovakve arhitekture sklopa: konceptualna sklopka S<sub>d</sub> koju ovdje razmatramo tamo je bila izgrađena od dvije paralelno spojene diode. Diodu bismo "uključili" spajanjem lijeve strane (pogledajte ponovno sliku sklopa) na nisku naponsku razinu i tada bi na izlazu sklopa bila niska naponska razina.</p>

<h3>Izlaz s otporom za pritezanje na masu</h3>

<p>Struktura ovakvog sklopa prikazana je na slici 2.</p>

<figure class="mlFigure">
<img src="@#file#(of-pd.svg)" alt="Izlaz pritegnut na masu."><br>
<figcaption><span class="mlFigKey">Slika 2.</span> Izlaz pritegnut na masu.</figcaption>
</figure>

<p>Kod ove izvedbe digitalnog sklopa izlaz <i>f</i> otpornikom za pritezanje spojen je na masu. Dodatno, izlaz je sklopkom S<sub>d</sub> povezan na napon napajanja. Radom sklopke na neki način upravljaju ulazi sklopa (koji ovdje nisu prikazani). Napon koji se pojavljuje na izlazu sklopa određen je izrazom:
$$
 U_f = I_R \cdot R
$$
gdje smo s \(I_R\) označili struju koja teče iz izvora napona <i>U</i> kroz otpornik <i>R</i> i koja prema Ohmovom zakonu na njemu stvara pad napona \(I_R \cdot R\). U situaciji kada je sklopka S<sub>d</sub> isključena, strujni krug je prekinut i ne teče struja. Stoga je \(I_R=0\) pa je \(U_f = I_R \cdot R = 0V\), odnosno napon na izlazu jednak je naponu mase (kažemo: pritegnut je na masu; od tuda i naziv ove izvedbe).
</p> 

<p>Ako sklopku S<sub>d</sub> uključimo, tada se ona (u idealnom slučaju) ponaša kao kratak spoj. Time će izlaz sklopa biti spojen na napajanje pa će na njemu biti visoka naponska razina (sklopkom smo potencijal napajanja direktno doveli na izlaz). Uočite da u ovom slučaju kroz sklop teče struja iznosa:
$$
 I_R = \frac{U_R}{R} = \frac{U_f}{R} = \frac{U}{R}.
$$
Kod sklopova ostvarenih na ovaj način postoji asimetrija: u jednoj situaciji struja ne teče pa sklop ne disipira snagu dok u drugoj situaciji struja teče i sklop disipira snagu.
</p>

<p>Interaktivna simulacija rada ovakvog sklopa prikazana je u nastavku. Klikom miša možete uključiti odnosno isključiti sklopku. Pogledajte kako se mijenjaju naponi na elementima te na izlazu. U ovom konkretnom slučaju simulacije, stanjem sklopke upravlja varijabla A: kada je A=0, sklopka je isključena a kada je A=1, sklopka je uključena. Kao posljedicu toga ovaj sklop realizira funkciju \(f(A)=A\). Kada bi neka druga kombinacija varijabli upravljala sklopkom, dobili bismo druge funkcije.</p>

<div style="text-align: center;">
<canvas id="pullDownResistorCircuitCanvas"  width="500" height="300" style="border: solid 1px black"></canvas><br>
<span class="mlSimAuthor">Autor simulacije: Leon Luttenberger</span>
</div>

<script>
(function(){
    var canvas = document.getElementById("pullDownResistorCircuitCanvas");
    var context = canvas.getContext("2d");

    var circuit = new PullDownResistorCircuit(0, 5);
    var truthTable = new TruthTable('1', '0', false);

    var fontSize = 20;

    repaint();

    function repaint() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        circuit.draw(context, 100, 25, 150, 250, true);

        context.fillStyle = "black";
        context.font =  fontSize + 'px Ariel';
        context.textBaseline = "middle";
        context.textAlign = "left";

        context.fillText(
                "f=" + (circuit.output ? '1' : '0'),
                circuit.outputConnector.x + 5,
                circuit.outputConnector.y
        );

        var selectedRow = circuit.input ? 1 : 0;
        truthTable.draw(context, 325, 40, 150, 125, selectedRow);
    }

    canvas.addEventListener("click", function (e) {
        var pos = getMousePos(canvas, e);

        if (circuit.transistorRectangle.containsPoint(pos.x, pos.y)) {
            circuit.setInput(!circuit.input);
            repaint();
        }

    }, false);
})();
</script> 

<p>Diodni sklop ILI primjer je ovakve arhitekture sklopa: konceptualna sklopka S<sub>d</sub> koju ovdje razmatramo tamo je bila izgrađena od dvije paralelno spojene diode. Diodu bismo "uključili" spajanjem lijeve strane (pogledajte ponovno sliku sklopa) na visoku naponsku razinu i tada bi na izlazu sklopa bila visoka naponska razina.</p>

<h3>Sklop s dvije sklopke prema napajanju i masi</h3>

<p>Struktura ovakvog sklopa prikazana je na slici 3.</p>

<figure class="mlFigure">
<img src="@#file#(of-s.svg)" alt="Sklop s dvije sklopke prema napajanju i masi."><br>
<figcaption><span class="mlFigKey">Slika 3.</span> Sklop s dvije sklopke prema napajanju i masi.</figcaption>
</figure>

<p>Kod ove izvedbe digitalnog sklopa izlaz <i>f</i> jednom je sklopkom (S<sub>u</sub>) povezan prema napajanju a drugom sklopkom (S<sub>d</sub>) prema masi. Radom obaju sklopki na neki način upravljaju ulazi sklopa (koji ovdje nisu prikazani). Pri tome je izuzetno važno osigurati da obje sklopke <b>nisu uključene istovremeno</b> - kada bi se to dogodilo, izvor napajanja kratko bismo spojili s masom što bi uzrokovalo visoku struju koja bi termički uništila sklop.</p> 

<p>Izlaz ovakvih sklopova može se konfigurirati na tri načina, kako je prikazano na slici u nastavku.</p>

<figure class="mlFigure">
<img src="@#file#(of-s-sl.svg)" alt="Sklop s dvije sklopke prema napajanju i masi: mogućnosti."><br>
<figcaption><span class="mlFigKey">Slika 4.</span> Sklop s dvije sklopke prema napajanju i masi: mogućnosti.</figcaption>
</figure>

<p>U prvom slučaju (slika 4.a), donja sklopka je uključena a gornja isključena. Time je izlaz spojen na masu pa je na njemu 0V. U drugom slučaju (slika 4.b), gornja sklopka je uključena a donja isključena. Time je izlaz spojen na izvor napajanja pa je napon na njemu jednak naponu izvora napajanja U.</p>

<p>Treći slučaj posebno je zanimljiv, a prikazan je na slici 4.c: obje sklopke su isključene čime je izlaz sklopa izoliran i od napajanja i od mase. Izlaz tog sklopa ni na koji način ne utječe na ostatak digitalnih sklopova na koje je spojen. Ovakvo stanje izlaza nazivamo stanjem visoke impedancije i označavamo slovom Z.</p>

<p>Slučaj koji je zabranjen prikazan je na slici 4.d: tu su obje sklopke uključene što bi u praksi brzo dovelo do pregaranja sklopa.</p>

<div class="mlNote"><p>Opisane arhitekture sklopova vidjet ćemo u nastavku: najprije na primjerima izvedbi invertora a potom i na primjerima složenijih funkcija.</p>
<p>Opisane arhitekture pomoći će nam i u razumijevanju vremena porasta i pada napona na izlazu sklopa. Ta su nam vremena od presudnog značaja jer izravno utječu na maksimalnu frekvenciju rada takvih sklopova.
</p></div>





