function TruthTable(trueSymbol, falseSymbol, inverter) {

    this.trueSymbol = trueSymbol;
    this.falseSymbol = falseSymbol;
    this.inverter = inverter;

    this.draw = function (context, xPos, yPos, width, height, highlightedRow) {
        context.clearRect(xPos, yPos, width, height);

        var columnWidth = width / 2;
        var rowHeight = height / 3;

        var headerLineY = yPos + rowHeight;
        var lastColumnSeparatorX = xPos + width - columnWidth;

        //highlight selected row
        highlightedRow++; //adjustment because the drawSymbol method indexes the header row with 0
        context.fillStyle = "yellow";
        context.fillRect(xPos, yPos + highlightedRow * rowHeight, width, rowHeight);

        //draw table layout
        context.strokeWidth = 2;
        context.moveTo(xPos, headerLineY);
        context.lineTo(xPos + width, headerLineY);
        context.stroke();

        context.moveTo(lastColumnSeparatorX, yPos);
        context.lineTo(lastColumnSeparatorX, yPos + height);
        context.stroke();

        context.fillStyle = "black";
        var fontSize = width /6;
        context.font = fontSize + "px Arial";

        function drawSymbol(symbol, row, column) {
            var x = xPos + column * columnWidth;
            var y = yPos + row * rowHeight;

            context.fillText(
                symbol,
                x + columnWidth / 2,
                y + rowHeight / 2
            );
        }

        var previousBaseline = context.textBaseline;
        var previousAlign = context.textAlign;

        context.textBaseline = "middle";
        context.textAlign = "center";

        drawSymbol('A', 0, 0);
        drawSymbol('f', 0, 1);

        drawSymbol(this.falseSymbol, 1, 0);
        drawSymbol(this.inverter ? this.trueSymbol : this.falseSymbol, 1, 1);
        drawSymbol(this.trueSymbol, 2, 0);
        drawSymbol(this.inverter ? this.falseSymbol : this.trueSymbol, 2, 1);

        context.textBaseline = previousBaseline;
        context.textAlign = previousAlign;
    };
}