/*
function TruthTable(trueSymbol, falseSymbol, inverter) {

    this.trueSymbol = trueSymbol;
    this.falseSymbol = falseSymbol;
    this.inverter = inverter;

    this.draw = function (context, xPos, yPos, width, height, highlightedRow) {
        context.clearRect(xPos, yPos, width, height);

        var columnWidth = width / 2;
        var rowHeight = height / 3;

        var headerLineY = yPos + rowHeight;
        var lastColumnSeparatorX = xPos + width - columnWidth;

        //highlight selected row
        highlightedRow++; //adjustment because the drawSymbol method indexes the header row with 0
        context.fillStyle = "yellow";
        context.fillRect(xPos, yPos + highlightedRow * rowHeight, width, rowHeight);

        //draw table layout
        context.strokeWidth = 2;
        context.moveTo(xPos, headerLineY);
        context.lineTo(xPos + width, headerLineY);
        context.stroke();

        context.moveTo(lastColumnSeparatorX, yPos);
        context.lineTo(lastColumnSeparatorX, yPos + height);
        context.stroke();

        context.fillStyle = "black";
        var fontSize = width /6;
        context.font = fontSize + "px Arial";

        function drawSymbol(symbol, row, column) {
            var x = xPos + column * columnWidth;
            var y = yPos + row * rowHeight;

            context.fillText(
                symbol,
                x + columnWidth / 2,
                y + rowHeight / 2
            );
        }

        var previousBaseline = context.textBaseline;
        var previousAlign = context.textAlign;

        context.textBaseline = "middle";
        context.textAlign = "center";

        drawSymbol('A', 0, 0);
        drawSymbol('f', 0, 1);

        drawSymbol(this.falseSymbol, 1, 0);
        drawSymbol(this.inverter ? this.trueSymbol : this.falseSymbol, 1, 1);
        drawSymbol(this.trueSymbol, 2, 0);
        drawSymbol(this.inverter ? this.falseSymbol : this.trueSymbol, 2, 1);

        context.textBaseline = previousBaseline;
        context.textAlign = previousAlign;
    };
}
*/
function PullUpResistorCircuit(lowVoltage, highVoltage) {

    this.highVoltage = highVoltage;
    this.lowVoltage = lowVoltage;
    this.sourceVoltage = highVoltage;

    this.voltageF = undefined;

    this.input = undefined;
    this.output = undefined;

    this.outputConnector = undefined;

    this.setInput = function(value) {
        this.input = !!value;
        this.output = !this.input;

        if (this.input) {
            this.voltageF = this.lowVoltage;
        } else {
            this.voltageF = this.highVoltage;
        }
    };

    this.setInput(false);

    this.draw = function (context, xPos, yPos, boxWidth, boxHeight, toAnimate) {
        context.clearRect(xPos, yPos, boxWidth, boxHeight);
        window.clearTimeout(this.timeoutID);

        context.fillStyle = "black";
        context.strokeStyle = "black";
        context.lineWidth = 1;

        var transistorX = xPos + (1 / 4) * boxWidth;
        var outputX = xPos + (3 / 4) * boxWidth;
        var outputY = yPos + (1 / 2) * boxHeight;

        var transistorHeight = boxHeight / 6;
        var transistorY = (outputY + yPos + boxHeight - transistorHeight) / 2;

        context.beginPath();
        context.moveTo(transistorX, yPos);
        context.lineTo(transistorX, transistorY);
        context.stroke();

        context.beginPath();
        context.moveTo(transistorX, transistorY + transistorHeight);
        context.lineTo(transistorX, yPos + boxHeight);
        context.stroke();

        context.beginPath();
        context.moveTo(transistorX, outputY);
        context.lineTo(outputX, outputY);
        context.stroke();

        //draw line joinings
        var connectionCircleRadius = 3;

        (new Circle(transistorX, outputY, connectionCircleRadius)).draw(context, true);

        context.fillStyle = "white";
        (new Circle(transistorX, transistorY, connectionCircleRadius)).draw(context, true);
        (new Circle(transistorX, transistorY + transistorHeight, connectionCircleRadius)).draw(context, true);
        (new Circle(outputX, outputY, connectionCircleRadius)).draw(context, true);

        //draw resistor
        var resistorHeight = boxHeight / 6;
        var resistorWidth = boxWidth / 12;
        var resistorRectangle = new Rectangle(
            transistorX - resistorWidth / 2,
            (yPos + outputY - resistorHeight) / 2,
            resistorWidth,
            resistorHeight
        );
        resistorRectangle.draw(context, true);

        //draw grounding
        var groundSymbolWidth = boxWidth / 8;
        context.lineWidth = 2;
        context.moveTo(transistorX - groundSymbolWidth / 2, yPos + boxHeight);
        context.lineTo(transistorX + groundSymbolWidth / 2, yPos + boxHeight);
        context.stroke();

        //draw voltage arrow
        context.lineWidth = 1;
        context.fillStyle = "black";

        var voltageTriangleBase = boxWidth / 12;
        var voltageTriangleHeight = boxHeight / 20;
        var voltageTriangleBaseY = yPos + voltageTriangleHeight;

        var voltageTriangle = new Triangle(
            transistorX - voltageTriangleBase / 2,
            voltageTriangleBaseY,
            transistorX + voltageTriangleBase / 2,
            voltageTriangleBaseY,
            transistorX,
            yPos
        );
        voltageTriangle.draw(context, true);

        //draw voltage values
        var fontSize = Math.min(boxHeight, boxWidth) / 9;
        context.font = fontSize + 'px Arial';
        context.fillStyle = "black";

        context.textAlign = "right";
        context.textBaseline = "middle";
        context.fillText(this.voltageF + 'V', transistorX - 5, outputY);

        context.textAlign = "left";
        context.fillText(this.highVoltage + 'V', transistorX + voltageTriangleBase, voltageTriangleBaseY);

        //draw voltage drops
        drawArrowUp(
            context,
            transistorX + resistorRectangle.height / 2,
            resistorRectangle.y + resistorRectangle.height + 5,
            resistorRectangle.height + 10
        );
        drawArrowUp(
            context,
            outputX,
            yPos + boxHeight,
            yPos + boxHeight - outputY - 15
        );
        context.beginPath();
        context.moveTo(outputX - groundSymbolWidth / 2, yPos + boxHeight);
        context.lineTo(outputX + groundSymbolWidth / 2, yPos + boxHeight);
        context.stroke();

        context.textAlign = "left";
        context.textBaseline = "middle";

        context.fillText(
            this.voltageF + 'V',
            outputX + 5,
            (yPos + boxHeight + outputY) / 2
        );
        var resistorVoltage = this.highVoltage - this.voltageF;
        context.fillText(
            resistorVoltage + 'V',
            resistorRectangle.x + resistorRectangle.width * 2 + 10,
            resistorRectangle.y + resistorRectangle.height / 2
        );

        //draw transistor
        var openTransistorXOffset = boxWidth / 14;
        if (this.input) { //closed transistor
            context.lineWidth = 3;
            context.beginPath();
            context.moveTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
            context.lineTo(transistorX, transistorY + connectionCircleRadius);
            context.stroke();
            context.lineWidth = 1;
        } else { //open transistor
            context.beginPath();
            context.moveTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
            context.lineTo(transistorX - openTransistorXOffset, transistorY + connectionCircleRadius);
            context.stroke();
        }
        this.transistorRectangle = new Rectangle(
            transistorX - openTransistorXOffset,
            transistorY,
            2 * openTransistorXOffset,
            transistorHeight
        );

        this.outputConnector = new Point(outputX, outputY);

        this.animate = function (counter) {
            //redraw static parts of lines
            if (this.input) {
                context.moveTo(transistorX, yPos + voltageTriangleHeight);
                context.lineTo(transistorX, resistorRectangle.y);
                context.stroke();

                context.moveTo(transistorX, resistorRectangle.y + resistorRectangle.height);
                context.lineTo(transistorX, outputY - connectionCircleRadius);
                context.stroke();

                context.moveTo(transistorX, outputY + connectionCircleRadius);
                context.lineTo(transistorX, transistorY - connectionCircleRadius);
                context.stroke();

                context.moveTo(transistorX, transistorY + connectionCircleRadius);
                context.lineTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
                context.stroke();

                context.moveTo(transistorX, transistorY + transistorHeight + connectionCircleRadius);
                context.lineTo(transistorX, yPos + boxHeight);
                context.stroke();
            }

            var offset = counter % 10;

            context.strokeStyle = "yellow";
            context.setLineDash([5, 5]);

            if (this.input) {
                context.beginPath();
                context.moveTo(transistorX, yPos + voltageTriangleHeight + offset);
                context.lineTo(transistorX, resistorRectangle.y);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, resistorRectangle.y + resistorRectangle.height + offset);
                context.lineTo(transistorX, outputY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, outputY + connectionCircleRadius + offset);
                context.lineTo(transistorX, transistorY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, transistorY + connectionCircleRadius + offset);
                context.lineTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, transistorY + transistorHeight + connectionCircleRadius + offset);
                context.lineTo(transistorX, yPos + boxHeight);
                context.stroke();
            }

            context.strokeStyle = "black";
            context.setLineDash([]);

            var that = this;
            this.timeoutID = window.setTimeout(function () {
                that.animate(offset + 1);
            }, 40);
        };

        if (toAnimate) {
            this.animate(0);
        }
    }
}

function PullDownResistorCircuit(lowVoltage, highVoltage) {

    this.highVoltage = highVoltage;
    this.lowVoltage = lowVoltage;
    this.sourceVoltage = highVoltage;

    this.voltageF = undefined;

    this.input = undefined;
    this.output = undefined;

    this.outputConnector = undefined;

    this.setInput = function(value) {
        this.input = !!value;
        this.output = this.input;

        if (this.input) {
            this.voltageF = this.highVoltage;
        } else {
            this.voltageF = this.lowVoltage;
        }
    };

    this.setInput(false);

    this.draw = function (context, xPos, yPos, boxWidth, boxHeight, toAnimate) {
        context.clearRect(xPos, yPos, boxWidth, boxHeight);
        window.clearTimeout(this.timeoutID);

        context.fillStyle = "black";
        context.strokeStyle = "black";
        context.lineWidth = 1;

        var transistorX = xPos + (1 / 4) * boxWidth;
        var outputX = xPos + (3 / 4) * boxWidth;
        var outputY = yPos + (1 / 2) * boxHeight;

        var transistorHeight = boxHeight / 6;
        var transistorY = (outputY + yPos - transistorHeight) / 2;

        context.beginPath();
        context.moveTo(transistorX, yPos);
        context.lineTo(transistorX, transistorY);
        context.stroke();

        context.beginPath();
        context.moveTo(transistorX, transistorY + transistorHeight);
        context.lineTo(transistorX, yPos + boxHeight);
        context.stroke();

        context.beginPath();
        context.moveTo(transistorX, outputY);
        context.lineTo(outputX, outputY);
        context.stroke();

        //draw line joinings
        var connectionCircleRadius = 3;

        (new Circle(transistorX, outputY, connectionCircleRadius)).draw(context, true);

        context.fillStyle = "white";
        (new Circle(transistorX, transistorY, connectionCircleRadius)).draw(context, true);
        (new Circle(transistorX, transistorY + transistorHeight, connectionCircleRadius)).draw(context, true);
        (new Circle(outputX, outputY, connectionCircleRadius)).draw(context, true);

        //draw resistor
        var resistorHeight = boxHeight / 6;
        var resistorWidth = boxWidth / 12;
        var resistorRectangle = new Rectangle(
            transistorX - resistorWidth / 2,
            (yPos + boxHeight + outputY - resistorHeight) / 2,
            resistorWidth,
            resistorHeight
        );
        resistorRectangle.draw(context, true);

        //draw grounding
        var groundSymbolWidth = boxWidth / 8;
        context.lineWidth = 2;
        context.moveTo(transistorX - groundSymbolWidth / 2, yPos + boxHeight);
        context.lineTo(transistorX + groundSymbolWidth / 2, yPos + boxHeight);
        context.stroke();

        //draw voltage arrow
        context.lineWidth = 1;
        context.fillStyle = "black";

        var voltageTriangleBase = boxWidth / 12;
        var voltageTriangleHeight = boxHeight / 20;
        var voltageTriangleBaseY = yPos + voltageTriangleHeight;

        var voltageTriangle = new Triangle(
            transistorX - voltageTriangleBase / 2,
            voltageTriangleBaseY,
            transistorX + voltageTriangleBase / 2,
            voltageTriangleBaseY,
            transistorX,
            yPos
        );
        voltageTriangle.draw(context, true);

        //draw voltage values
        var fontSize = Math.min(boxHeight, boxWidth) / 9;
        context.font = fontSize + 'px Arial';
        context.fillStyle = "black";

        context.textAlign = "right";
        context.textBaseline = "middle";
        context.fillText(this.voltageF + 'V', transistorX - 5, outputY);

        context.textAlign = "left";
        context.fillText(this.highVoltage + 'V', transistorX + voltageTriangleBase, voltageTriangleBaseY);

        //draw voltage drops
        drawArrowUp(
            context,
            transistorX + resistorRectangle.height / 2,
            resistorRectangle.y + resistorRectangle.height + 5,
            resistorRectangle.height + 10
        );
        drawArrowUp(
            context,
            outputX,
            yPos + boxHeight,
            yPos + boxHeight - outputY - 15
        );
        context.beginPath();
        context.moveTo(outputX - groundSymbolWidth / 2, yPos + boxHeight);
        context.lineTo(outputX + groundSymbolWidth / 2, yPos + boxHeight);
        context.stroke();

        context.textAlign = "left";
        context.textBaseline = "middle";

        context.fillText(
            this.voltageF + 'V',
            outputX + 5,
            (yPos + boxHeight + outputY) / 2
        );
        var resistorVoltage = this.voltageF;
        context.fillText(
            resistorVoltage + 'V',
            resistorRectangle.x + resistorRectangle.width * 2 + 10,
            resistorRectangle.y + resistorRectangle.height / 2
        );

        //draw transistor
        var openTransistorXOffset = boxWidth / 14;
        if (this.input) { //closed transistor
            context.lineWidth = 3;
            context.beginPath();
            context.moveTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
            context.lineTo(transistorX, transistorY + connectionCircleRadius);
            context.stroke();
            context.lineWidth = 1;
        } else { //open transistor
            context.beginPath();
            context.moveTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
            context.lineTo(transistorX - openTransistorXOffset, transistorY + connectionCircleRadius);
            context.stroke();
        }
        this.transistorRectangle = new Rectangle(
            transistorX - openTransistorXOffset,
            transistorY,
            2 * openTransistorXOffset,
            transistorHeight
        );

        this.outputConnector = new Point(outputX, outputY);

        this.animate = function (counter) {
            //redraw static parts of lines
            if (this.input) {
                context.moveTo(transistorX, yPos + voltageTriangleHeight);
                context.lineTo(transistorX, transistorY - connectionCircleRadius);
                context.stroke();

                context.moveTo(transistorX, transistorY + connectionCircleRadius);
                context.lineTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
                context.stroke();

                context.moveTo(transistorX, transistorY + transistorHeight + connectionCircleRadius);
                context.lineTo(transistorX, outputY - connectionCircleRadius);
                context.stroke();

                context.moveTo(transistorX, outputY + connectionCircleRadius);
                context.lineTo(transistorX, resistorRectangle.y);
                context.stroke();

                context.moveTo(transistorX, resistorRectangle.y + resistorRectangle.height);
                context.lineTo(transistorX, yPos + boxHeight);
                context.stroke();
            }

            var offset = counter % 10;

            context.strokeStyle = "yellow";
            context.setLineDash([5, 5]);

            if (this.input) {
                context.beginPath();
                context.moveTo(transistorX, yPos + voltageTriangleHeight + offset);
                context.lineTo(transistorX, transistorY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, transistorY + connectionCircleRadius + offset);
                context.lineTo(transistorX, transistorY + transistorHeight - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, transistorY + transistorHeight + connectionCircleRadius + offset);
                context.lineTo(transistorX, outputY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, outputY + connectionCircleRadius + offset);
                context.lineTo(transistorX, resistorRectangle.y);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, resistorRectangle.y + resistorRectangle.height + offset);
                context.lineTo(transistorX, yPos + boxHeight);
                context.stroke();
            }

            context.strokeStyle = "black";
            context.setLineDash([]);

            var that = this;
            this.timeoutID = window.setTimeout(function () {
                that.animate(offset + 1);
            }, 40);
        };

        if (toAnimate) {
            this.animate(0);
        }
    }
}

function NPNInverterCircuit(lowVoltage, highVoltage, resistorVoltage, drainSourceVoltage) {

    this.highVoltage = highVoltage;
    this.lowVoltage = lowVoltage;
    this.resistorVoltage = resistorVoltage;
    this.drainSourceVoltage = drainSourceVoltage;

    this.sourceVoltage = undefined;
    this.voltageF = undefined;

    this.input = undefined;
    this.output = undefined;

    this.inputConnector = undefined;
    this.outputConnector = undefined;

    this.setInput = function (value) {
        this.input = !!value;
        this.output = !this.input;

        if (this.input) {
            this.voltageF = this.drainSourceVoltage;
        } else {
            this.voltageF = this.highVoltage;
        }
    };

    this.setInput(false);

    this.draw = function (context, xPos, yPos, boxWidth, boxHeight, toAnimate) {
        context.clearRect(xPos, yPos, boxWidth, boxHeight);
        window.clearTimeout(this.timeoutID);

        context.fillStyle = "black";
        context.strokeStyle = "black";
        context.lineWidth = 1;

        var transistorX = xPos + (4/7) * boxWidth;
        var voltageLineX = xPos + (5/7) * boxWidth;
        var inputY = yPos + (4/7) * boxHeight;
        var outputY = yPos + (3/7) * boxHeight;

        var transistorHeight = (inputY - outputY) * 2;

        //draw lines
        context.beginPath();

        context.moveTo(xPos, inputY);
        context.lineTo(transistorX, inputY);

        context.moveTo(transistorX, inputY - transistorHeight / 4);
        context.lineTo(voltageLineX, outputY);

        context.moveTo(voltageLineX, outputY);
        context.lineTo(xPos + boxWidth, outputY);

        context.moveTo(transistorX, inputY + transistorHeight / 4);
        context.lineTo(voltageLineX, inputY + transistorHeight / 2);

        context.moveTo(voltageLineX, inputY + transistorHeight / 2);
        context.lineTo(voltageLineX, yPos + boxHeight);

        context.moveTo(voltageLineX, outputY);
        context.lineTo(voltageLineX, yPos);

        context.stroke();

        //draw transistor
        context.lineWidth = 2;

        context.beginPath();
        context.moveTo(transistorX, inputY - transistorHeight / 2);
        context.lineTo(transistorX, inputY + transistorHeight / 2);
        context.stroke();

        //draw grounding
        var groundSymbolWidth = boxWidth / 6;

        context.beginPath();
        context.moveTo(voltageLineX - groundSymbolWidth / 2, yPos + boxHeight);
        context.lineTo(voltageLineX + groundSymbolWidth / 2, yPos + boxHeight);
        context.stroke();

        context.lineWidth = 1;

        //draw emitter arrow
        context.beginPath();

        context.moveTo(voltageLineX, inputY + transistorHeight / 2);
        context.lineTo(voltageLineX - 2, inputY + transistorHeight / 2 - 10);

        context.moveTo(voltageLineX, inputY + transistorHeight / 2);
        context.lineTo(voltageLineX - 10, inputY + transistorHeight  / 2 - 2);

        context.stroke();

        //draw resistors
        var resistorLength = Math.min(boxHeight, boxWidth) / 5;
        var resistorWidth = resistorLength / 3;
        context.fillStyle = "white";

        var inputRectangle = new Rectangle(
            (transistorX + xPos - resistorLength) / 2,
            inputY - resistorWidth / 2,
            resistorLength,
            resistorWidth
        );

        context.beginPath();
        inputRectangle.draw(context, true);

        var voltageResistor = new Rectangle(
            voltageLineX - resistorWidth / 2,
            (yPos + outputY - resistorLength) / 2,
            resistorWidth,
            resistorLength
        );

        context.beginPath();
        voltageResistor.draw(context, true);

        //draw voltage triangle
        context.fillStyle = "black";

        var voltageTriangleHeight = (outputY - yPos) / 6;
        var voltageTriangleBase = voltageTriangleHeight / 2;
        var voltageTriangle = new Triangle(
            voltageLineX,
            yPos,
            voltageLineX - voltageTriangleBase / 2,
            yPos + voltageTriangleHeight,
            voltageLineX + voltageTriangleBase / 2,
            yPos + voltageTriangleHeight
        );

        context.beginPath();
        voltageTriangle.draw(context, true);

        //draw connection circles
        var connectionCircleRadius = 3;

        (new Circle(voltageLineX, outputY, connectionCircleRadius)).draw(context, true);

        context.fillStyle = "white";
        context.beginPath();
        (new Circle(xPos, inputY, connectionCircleRadius)).draw(context, true);
        context.beginPath();
        (new Circle(xPos + boxWidth, outputY, connectionCircleRadius)).draw(context, true);

        this.inputConnector = new Point(xPos, inputY);
        this.outputConnector = new Point(xPos + boxWidth, outputY);

        this.inputConnector.x -= connectionCircleRadius;
        this.outputConnector.x += connectionCircleRadius;

        var fontSize = Math.min(boxHeight, boxWidth) / 9;
        context.font = fontSize + 'px Arial';
        context.fillStyle = "black";

        //voltage source value
        context.textBaseline = "middle";
        context.textAlign = "left";
        context.fillText(
            this.highVoltage + 'V',
            voltageLineX + voltageTriangleBase,
            yPos + (voltageTriangleHeight / 2)
        );

        //draw voltage drops
        var inputVoltage = this.input ? this.highVoltage : this.lowVoltage;

        context.textBaseline = "middle";
        context.textAlign = "right";
        context.fillText(inputVoltage + 'V', xPos + 10, (inputY + yPos + boxHeight) / 2);
        drawArrowUp(
            context,
            xPos + 12,
            yPos + boxHeight,
            yPos + boxHeight - inputY - 10
        );

        var baseVoltage = this.input ? (this.highVoltage - this.resistorVoltage) : this.lowVoltage;

        context.textBaseline = "middle";
        context.textAlign = "right";
        context.fillText(
            baseVoltage.toFixed(1) + 'V',
            (inputRectangle.x + inputRectangle.width + voltageLineX) / 2 - 5,
            (inputRectangle.y + inputRectangle.height + yPos + boxHeight) / 2
        );
        drawArrow(
            context,
            inputRectangle.x + inputRectangle.width + 10,
            inputRectangle.y + inputRectangle.height + 10,
            voltageLineX - 10,
            yPos + boxHeight - 10
        );

        context.textBaseline = "middle";
        context.textAlign = "left";
        context.fillText(
            this.voltageF + 'V',
            xPos + boxWidth - 8,
            (outputY + yPos + boxHeight) / 2
        );
        drawArrowUp(
            context,
            xPos + boxWidth - 10,
            yPos + boxHeight,
            yPos + boxHeight - outputY - 10
        );

        drawArrowUp(
            context,
            voltageResistor.x + voltageResistor.width + 10,
            voltageResistor.y + voltageResistor.height + 5,
            voltageResistor.height + 15
        );
        drawArrowLeft(
            context,
            inputRectangle.x + inputRectangle.width + 5,
            inputRectangle.y - 10,
            inputRectangle.width + 15
        );

        //input resistor voltages
        context.textBaseline = "bottom";
        context.textAlign = "center";
        context.fillText( //input resistor
            (this.input ? this.resistorVoltage : 0) + 'V',
            inputRectangle.x + inputRectangle.width / 2,
            inputRectangle.y - 15
        );

        context.textBaseline = "middle";
        context.textAlign = "left";
        context.fillText( //voltage resistor
            (this.highVoltage - this.voltageF) + 'V',
            voltageResistor.x + voltageResistor.width + 15,
            voltageResistor.y + voltageResistor.height / 2
        );

        //animations
        this.animate = function (counter) {

            var wireToTransistorLength = Math.sqrt(
                Math.pow(voltageLineX - transistorX, 2)
                + Math.pow(transistorHeight / 4, 2)
            );
            var wireXRatio = (voltageLineX - transistorX) / wireToTransistorLength;
            var wireYRatio = (transistorHeight / 4) / wireToTransistorLength;

            //redraw static parts of the lines
            if (this.input) {
                context.beginPath();
                context.moveTo(voltageLineX, yPos + voltageTriangleHeight);
                context.lineTo(voltageLineX, voltageResistor.y);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, voltageResistor.y + voltageResistor.height);
                context.lineTo(voltageLineX, outputY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX - connectionCircleRadius * wireXRatio, outputY + connectionCircleRadius * wireYRatio);
                context.lineTo(transistorX, inputY - transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, inputY + transistorHeight / 4);
                context.lineTo(voltageLineX, inputY + transistorHeight / 2);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, inputY + transistorHeight / 2);
                context.lineTo(voltageLineX, yPos + boxHeight);
                context.stroke();

                context.beginPath();
                context.moveTo(xPos + connectionCircleRadius, inputY);
                context.lineTo(inputRectangle.x, inputY);
                context.stroke();

                context.beginPath();
                context.moveTo(inputRectangle.x + inputRectangle.width, inputY);
                context.lineTo(transistorX, inputY);
                context.stroke();
            }

            var offset = counter % 10;

            context.strokeStyle = "yellow";
            context.setLineDash([5, 5]);

            //draw moving yellow lines
            if (this.input) {
                context.beginPath();
                context.moveTo(voltageLineX, yPos + offset + voltageTriangleHeight);
                context.lineTo(voltageLineX, voltageResistor.y);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, voltageResistor.y + voltageResistor.height + offset);
                context.lineTo(voltageLineX, outputY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(
                    voltageLineX - (connectionCircleRadius + offset) * wireXRatio,
                    outputY + (connectionCircleRadius + offset) * wireYRatio
                );
                context.lineTo(transistorX, inputY - transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(
                    transistorX + offset * wireXRatio,
                    inputY + transistorHeight / 4 + offset * wireYRatio
                );
                context.lineTo(voltageLineX, inputY + transistorHeight / 2);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, inputY + transistorHeight / 2 + offset);
                context.lineTo(voltageLineX, yPos + boxHeight);
                context.stroke();

                context.beginPath();
                context.moveTo(xPos + connectionCircleRadius + offset, inputY);
                context.lineTo(inputRectangle.x, inputY);
                context.stroke();

                context.beginPath();
                context.moveTo(inputRectangle.x + inputRectangle.width + offset, inputY);
                context.lineTo(transistorX, inputY);
                context.stroke();
            }

            context.strokeStyle = "black";
            context.setLineDash([]);

            var that = this;
            this.timeoutID = window.setTimeout(function () {
                that.animate(offset + 1);
            }, 40);
        };

        if (toAnimate) {
            this.animate(0);
        }
    };
}

function NMOSInverterCircuit(lowVoltage, highVoltage, drainSourceVoltage) {

    this.highVoltage = highVoltage;
    this.lowVoltage = lowVoltage;
    this.drainSourceVoltage = drainSourceVoltage;

    this.sourceVoltage = undefined;
    this.voltageF = undefined;

    this.input = undefined;
    this.output = undefined;

    this.inputConnector = undefined;
    this.outputConnector = undefined;

    this.setInput = function (value) {
        this.input = !!value;
        this.output = !this.input;

        if (this.input) {
            this.voltageF = this.drainSourceVoltage;
        } else {
            this.voltageF = this.highVoltage;
        }
    };

    this.setInput(false);

    this.draw = function (context, xPos, yPos, boxWidth, boxHeight, toAnimate) {
        context.clearRect(xPos, yPos, boxWidth, boxHeight);
        window.clearTimeout(this.timeoutID);

        context.fillStyle = "black";
        context.strokeStyle = "black";
        context.lineWidth = 1;

        var transistorX = xPos + (3/7) * boxWidth;
        var voltageLineX = xPos + (5/7) * boxWidth;
        var inputY = yPos + (3/4) * boxHeight;
        var outputY = yPos + (1/2) * boxHeight;

        var transistorHeight = inputY - outputY;
        var transistorSpacing = boxWidth / 24;

        //draw lines
        context.beginPath();

        context.moveTo(xPos, inputY);
        context.lineTo(transistorX - transistorSpacing, inputY);

        context.moveTo(transistorX, inputY - transistorHeight / 4);
        context.lineTo(voltageLineX, inputY - transistorHeight / 4);

        context.moveTo(voltageLineX, inputY - transistorHeight / 4);
        context.lineTo(voltageLineX, outputY);

        context.moveTo(transistorX, inputY + transistorHeight / 4);
        context.lineTo(voltageLineX, inputY + transistorHeight / 4);

        context.moveTo(voltageLineX, inputY + transistorHeight / 4);
        context.lineTo(voltageLineX, yPos + boxHeight);

        context.moveTo(voltageLineX, outputY);
        context.lineTo(xPos + boxWidth, outputY);

        context.moveTo(voltageLineX, outputY);
        context.lineTo(voltageLineX, yPos);

        context.stroke();

        //draw transistor
        context.lineWidth = 2;

        context.beginPath();
        context.moveTo(transistorX, inputY - transistorHeight / 2);
        context.lineTo(transistorX, inputY + transistorHeight / 2);

        context.moveTo(transistorX - transistorSpacing, inputY - transistorHeight / 3);
        context.lineTo(transistorX - transistorSpacing, inputY + transistorHeight / 3);

        context.stroke();

        //draw grounding
        var groundSymbolWidth = boxWidth / 6;

        context.beginPath();
        context.moveTo(voltageLineX - groundSymbolWidth / 2, yPos + boxHeight);
        context.lineTo(voltageLineX + groundSymbolWidth / 2, yPos + boxHeight);
        context.stroke();

        context.lineWidth = 1;

        //draw emitter arrow
        drawArrowRight(
            context,
            transistorX,
            inputY + transistorHeight / 4,
            voltageLineX - transistorX
        );

        //draw resistors
        var resistorLength = Math.min(boxHeight, boxWidth) / 5;
        var resistorWidth = resistorLength / 3;
        context.fillStyle = "white";

        var voltageResistor = new Rectangle(
            voltageLineX - resistorWidth / 2,
            (yPos + outputY - resistorLength) / 2,
            resistorWidth,
            resistorLength
        );

        context.beginPath();
        voltageResistor.draw(context, true);

        //draw voltage triangle
        context.fillStyle = "black";

        var voltageTriangleHeight = (outputY - yPos) / 6;
        var voltageTriangleBase = voltageTriangleHeight / 2;
        var voltageTriangle = new Triangle(
            voltageLineX,
            yPos,
            voltageLineX - voltageTriangleBase / 2,
            yPos + voltageTriangleHeight,
            voltageLineX + voltageTriangleBase / 2,
            yPos + voltageTriangleHeight
        );

        context.beginPath();
        voltageTriangle.draw(context, true);

        //draw connection circles
        var connectionCircleRadius = 3;

        (new Circle(voltageLineX, outputY, connectionCircleRadius)).draw(context, true);

        context.fillStyle = "white";
        context.beginPath();
        (new Circle(xPos, inputY, connectionCircleRadius)).draw(context, true);
        context.beginPath();
        (new Circle(xPos + boxWidth, outputY, connectionCircleRadius)).draw(context, true);

        this.inputConnector = new Point(xPos, inputY);
        this.outputConnector = new Point(xPos + boxWidth, outputY);

        this.inputConnector.x -= connectionCircleRadius;
        this.outputConnector.x += connectionCircleRadius;

        //draw voltage values
        var fontSize = Math.min(boxHeight, boxWidth) / 9;
        context.font = fontSize + 'px Arial';
        context.fillStyle = "black";

        context.textBaseline = "middle";
        context.textAlign = "left";
        context.fillText(
            this.highVoltage + 'V',
            voltageLineX + voltageTriangleBase,
            yPos + (voltageTriangleHeight / 2)
        );

        //draw voltage drops
        context.textBaseline = "middle";
        context.textAlign = "left";
        context.fillText(
            (this.highVoltage - this.voltageF) + 'V',
            voltageResistor.x + voltageResistor.width + 12,
            voltageResistor.y + voltageResistor.height / 2
        );
        drawArrowUp(
            context,
            voltageResistor.x + voltageResistor.width + 10,
            voltageResistor.y + voltageResistor.height + 5,
            voltageResistor.height + 15
        );

        context.textBaseline = "middle";
        context.textAlign = "left";
        context.fillText(
            this.voltageF + 'V',
            (voltageLineX + xPos + boxWidth) / 2,
            (outputY + yPos + boxHeight) / 2
        );
        drawArrowUp(
            context,
            (voltageLineX + xPos + boxWidth) / 2 - 2,
            yPos + boxHeight - 10,
            yPos + boxHeight - outputY - 20
        );

        var inputVoltage = this.input ? this.highVoltage : this.lowVoltage;
        context.textBaseline = "middle";
        context.textAlign = "right";
        context.fillText(
            inputVoltage + 'V',
            (xPos + transistorX) / 2 - 2,
            (inputY + yPos + boxHeight) / 2
        );
        drawArrowUp(
            context,
            (xPos + transistorX) / 2,
            yPos + boxHeight - 10,
            yPos + boxHeight - inputY - 20
        );

        //animations
        this.animate = function (counter) {

            //redraw static parts of the lines
            if (this.input) {
                context.beginPath();
                context.moveTo(voltageLineX, yPos + voltageTriangleHeight);
                context.lineTo(voltageLineX, voltageResistor.y);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, voltageResistor.y + voltageResistor.height);
                context.lineTo(voltageLineX, outputY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, outputY + connectionCircleRadius);
                context.lineTo(voltageLineX, inputY - transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, inputY - transistorHeight / 4);
                context.lineTo(transistorX, inputY - transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX, inputY + transistorHeight / 4);
                context.lineTo(voltageLineX, inputY + transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, inputY + transistorHeight / 4);
                context.lineTo(voltageLineX, yPos + boxHeight);
                context.stroke();
            }

            var offset = counter % 10;

            context.strokeStyle = "yellow";
            context.setLineDash([5, 5]);

            //draw moving yellow lines
            if (this.input) {
                context.beginPath();
                context.moveTo(voltageLineX, yPos + voltageTriangleHeight + offset);
                context.lineTo(voltageLineX, voltageResistor.y);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, voltageResistor.y + voltageResistor.height + offset);
                context.lineTo(voltageLineX, outputY - connectionCircleRadius);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, outputY + connectionCircleRadius + offset);
                context.lineTo(voltageLineX, inputY - transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX - offset, inputY - transistorHeight / 4);
                context.lineTo(transistorX, inputY - transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(transistorX + offset, inputY + transistorHeight / 4);
                context.lineTo(voltageLineX, inputY + transistorHeight / 4);
                context.stroke();

                context.beginPath();
                context.moveTo(voltageLineX, inputY + transistorHeight / 4 + offset);
                context.lineTo(voltageLineX, yPos + boxHeight);
                context.stroke();
            }

            context.strokeStyle = "black";
            context.setLineDash([]);

            var that = this;
            this.timeoutID = window.setTimeout(function () {
                that.animate(offset + 1);
            }, 40);
        };

        if (toAnimate) {
            this.animate(0);
        }
    };
}
