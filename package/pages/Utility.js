function Point (x, y) {
    this.x = x;
    this.y = y;
};

var Circle = function Circle (x, y, radius) {
    this.x = x;
    this.y = y;
    this.radius = radius;

    this.containsPoint = function (x, y) {
        var distance = Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2));
        return distance <= this.radius;
    };

    this.draw = function (context, filled) {
        context.beginPath();
        context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        context.stroke();

        if (filled) {
            context.fill();
        }
    }
};

function Rectangle(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.containsPoint  = function (x, y) {
        if (x < this.x) return false;
        if (x > (this.x + this.width)) return false;
        if (y < this.y) return false;
        if (y > this.y + this.height) return false;

        return true;
    };

    this.draw = function(context, filled) {
        context.strokeRect(this.x, this.y, this.width, this.height);

        if (filled) {
            context.fillRect(this.x, this.y, this.width, this.height);
        }
    };
}

function Triangle (x1, y1, x2, y2, x3, y3) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.x3 = x3;
    this.y3 = y3;

    this.draw = function (context, filled) {
        context.beginPath();
        context.moveTo(this.x1, this.y1);
        context.lineTo(this.x2, this.y2);
        context.lineTo(this.x3, this.y3);
        context.closePath();

        context.stroke();

        if (filled) {
            context.fill();
        }
    };
}

function drawArrowUp(context, x, y, length) {
    var arrowHeadLength = length / 4;
    var arrowHeadWidth = arrowHeadLength;

    context.beginPath();

    context.moveTo(x, y);
    context.lineTo(x, y - length);

    context.moveTo(x - arrowHeadWidth / 2, y - length + arrowHeadLength);
    context.lineTo(x, y - length);

    context.moveTo(x + arrowHeadWidth / 2, y - length + arrowHeadLength);
    context.lineTo(x, y - length);

    context.stroke();
}

function drawArrowRight(context, x, y, length) {
    var arrowHeadLength = length / 3;
    var arrowHeadWidth = arrowHeadLength;

    context.beginPath();

    context.moveTo(x, y);
    context.lineTo(x + length, y);

    context.moveTo(x + length - arrowHeadLength, y - arrowHeadWidth / 2);
    context.lineTo(x + length, y);

    context.moveTo(x + length - arrowHeadLength, y + arrowHeadWidth / 2);
    context.lineTo(x + length, y);

    context.stroke();
}

function drawArrowLeft(context, x, y, length) {
    var arrowHeadLength = length / 3;
    var arrowHeadWidth = arrowHeadLength;

    context.beginPath();

    context.moveTo(x, y);
    context.lineTo(x - length, y);

    context.moveTo(x - length + arrowHeadLength, y - arrowHeadWidth / 2);
    context.lineTo(x - length, y);

    context.moveTo(x - length + arrowHeadLength, y + arrowHeadWidth / 2);
    context.lineTo(x - length, y);

    context.stroke();
}

function drawArrow(context, startX, startY, endX, endY) {
    var distanceX = endX - startX;
    var distanceY = endY - startY;

    var angle = Math.atan2(distanceX, distanceY);

    var arrowLength = 10;

    context.beginPath();

    context.moveTo(startX, startY);
    context.lineTo(endX, endY);

    context.moveTo(startX, startY);
    context.lineTo(startX + arrowLength * Math.cos(angle), startY + arrowLength * Math.sin(angle));

    context.moveTo(startX, startY);
    context.lineTo(startX + arrowLength * Math.cos(angle + Math.PI / 2), startY + arrowLength * Math.sin(angle + Math.PI / 2));

    context.stroke();
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function roundToDecimals(number, decimals) {
    return +(number.toFixed(decimals));
}
